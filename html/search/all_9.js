var searchData=
[
  ['machine_0',['machine',['../struct_p_e_header.html#a5ddefb78020e9dd399d269d24330b51b',1,'PEHeader']]],
  ['magic_1',['magic',['../struct_optional_header.html#a8c61e64b8675498cee79c59d3f8131e2',1,'OptionalHeader::magic()'],['../struct_p_e_file.html#a57f54349f4fd1cbbb52058812e146af2',1,'PEFile::magic()']]],
  ['magic_5foffset_2',['magic_offset',['../struct_p_e_file.html#ac8ca037a7c3416b4d3af1752e969ef2b',1,'PEFile']]],
  ['main_3',['main',['../solution_2src_2main_8c.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main.c']]],
  ['main_2ec_4',['main.c',['../solution_2src_2main_8c.html',1,'']]],
  ['major_5fimage_5fversion_5',['major_image_version',['../struct_optional_header.html#a674474b3f80d671f4a24f74a9e41cbad',1,'OptionalHeader']]],
  ['major_5flinker_5fversion_6',['major_linker_version',['../struct_optional_header.html#a310f531cee8af3d9d2c7cfafd32a091b',1,'OptionalHeader']]],
  ['major_5foperating_5fsystem_5fversion_7',['major_operating_system_version',['../struct_optional_header.html#a80e2162618e9355eca7e230e57b56469',1,'OptionalHeader']]],
  ['major_5fsubsystem_5fversion_8',['major_subsystem_version',['../struct_optional_header.html#a37be4eb1cef1a8a115828d460e273d4d',1,'OptionalHeader']]],
  ['minor_5fimage_5fversion_9',['minor_image_version',['../struct_optional_header.html#a87568eb425db11b7dcfdb1d44cd89fc2',1,'OptionalHeader']]],
  ['minor_5flinker_5fversion_10',['minor_linker_version',['../struct_optional_header.html#a5214827341b3f56d9451fa6b6afa10a6',1,'OptionalHeader']]],
  ['minor_5foperating_5fsystem_5fversion_11',['minor_operating_system_version',['../struct_optional_header.html#a9dc3f886310cb1c17180698c1c91b44e',1,'OptionalHeader']]],
  ['minor_5fsubsystem_5fversion_12',['minor_subsystem_version',['../struct_optional_header.html#a67b4f86ddd25088cb73ca5b36f14e8bd',1,'OptionalHeader']]]
];
