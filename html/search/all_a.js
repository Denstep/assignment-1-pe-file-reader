var searchData=
[
  ['name_0',['name',['../struct_section_header.html#aaf1bd07e95aeff0f585394fccaf479ab',1,'SectionHeader']]],
  ['number_5fof_5flinenumbers_1',['number_of_linenumbers',['../struct_section_header.html#a649a669c0dca94edb6a736c8ca9da92f',1,'SectionHeader']]],
  ['number_5fof_5frelocations_2',['number_of_relocations',['../struct_section_header.html#aa15228e6b5943592cbee6ff663c22102',1,'SectionHeader']]],
  ['number_5fof_5frva_5fand_5fsizes_3',['number_of_rva_and_sizes',['../struct_optional_header.html#a0d9e266bb7655a0897c8ac9b13a3565d',1,'OptionalHeader']]],
  ['number_5fof_5fsections_4',['number_of_sections',['../struct_p_e_header.html#a7b1fe607032ce6644df6710edf24356a',1,'PEHeader']]],
  ['number_5fof_5fsymbols_5',['number_of_symbols',['../struct_p_e_header.html#a403c2d6c879f8278811cb6bb36947af0',1,'PEHeader']]]
];
