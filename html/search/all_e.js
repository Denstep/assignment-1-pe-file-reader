var searchData=
[
  ['save_5fsection_0',['save_section',['../sections_8h.html#aa0e48fb8842be093b193f76b9b3a54f7',1,'save_section(FILE *file_input, FILE *file_output, struct SectionHeader *section_header):&#160;sections.c'],['../sections_8c.html#aa0e48fb8842be093b193f76b9b3a54f7',1,'save_section(FILE *file_input, FILE *file_output, struct SectionHeader *section_header):&#160;sections.c']]],
  ['section_5falignment_1',['section_alignment',['../struct_optional_header.html#ac85956d21ee1883ac25f3c1b9047af25',1,'OptionalHeader']]],
  ['section_5ffind_2',['section_find',['../sections_8h.html#ade21ce4b0404c8ca21c4efffc367408f',1,'section_find(struct PEFile *pe_file, char *name):&#160;sections.c'],['../sections_8c.html#ade21ce4b0404c8ca21c4efffc367408f',1,'section_find(struct PEFile *pe_file, char *name):&#160;sections.c']]],
  ['section_5fheader_5foffset_3',['section_header_offset',['../struct_p_e_file.html#a2813333b171b9d7326bdc7312319b649',1,'PEFile']]],
  ['section_5fheaders_4',['section_headers',['../struct_p_e_file.html#a945c299be847f4938a6b108b9d791252',1,'PEFile']]],
  ['sectionheader_5',['SectionHeader',['../struct_section_header.html',1,'']]],
  ['sections_2ec_6',['sections.c',['../sections_8c.html',1,'']]],
  ['sections_2eh_7',['sections.h',['../sections_8h.html',1,'']]],
  ['size_5fof_5fcode_8',['size_of_code',['../struct_optional_header.html#abec85eaa855fffedd0f1cc056d4be409',1,'OptionalHeader']]],
  ['size_5fof_5fheaders_9',['size_of_headers',['../struct_optional_header.html#ab4f68c5cf15ad5b5b07ce19158c634dc',1,'OptionalHeader']]],
  ['size_5fof_5fheap_5fcommit_10',['size_of_heap_commit',['../struct_optional_header.html#a2c9a883e9645583f9e1ea612a73093f1',1,'OptionalHeader']]],
  ['size_5fof_5fheap_5freserve_11',['size_of_heap_reserve',['../struct_optional_header.html#a09b955effd279c4308b383c6a6f0e368',1,'OptionalHeader']]],
  ['size_5fof_5fimage_12',['size_of_image',['../struct_optional_header.html#a26524493619181611d7806bb9e777001',1,'OptionalHeader']]],
  ['size_5fof_5finitialized_5fdata_13',['size_of_initialized_data',['../struct_optional_header.html#a84873e225f5bc9b5198dd1caea6d5c29',1,'OptionalHeader']]],
  ['size_5fof_5foptional_5fheader_14',['size_of_optional_header',['../struct_p_e_header.html#a769e761e08bc9235f2f7772ab11bb72f',1,'PEHeader']]],
  ['size_5fof_5fraw_5fdata_15',['size_of_raw_data',['../struct_section_header.html#ae591b2904f957453e485227a5a72ae1e',1,'SectionHeader']]],
  ['size_5fof_5fstack_5fcommit_16',['size_of_stack_commit',['../struct_optional_header.html#a7f933173dcd0b835cc73dd00db74a309',1,'OptionalHeader']]],
  ['size_5fof_5fstack_5freserve_17',['size_of_stack_reserve',['../struct_optional_header.html#a0baa0ee0706c5b99aeb6a58d60f3f735',1,'OptionalHeader']]],
  ['size_5fof_5funinitialized_5fdata_18',['size_of_uninitialized_data',['../struct_optional_header.html#a86db8aaeb663ce286452b0691e4eeda0',1,'OptionalHeader']]],
  ['subsystem_19',['subsystem',['../struct_optional_header.html#a83a7799c8c421b869bd0f16d1dbba44d',1,'OptionalHeader']]]
];
