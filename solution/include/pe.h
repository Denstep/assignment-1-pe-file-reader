/// @file
/// @brief PE read structure
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/// Structure File Header
struct
#ifdef _MSC_VER
    #pragma packed(push, 1)
#else // _MSC_VER
    __attribute__((packed))
#endif
PEHeader
{
    /// @name File Header
	///@{

	///The number that identifies the type of target machine. For more information, see Machine Types.
	uint16_t machine;
	///The number of sections. This indicates the size of the section table, which immediately follows the headers.
	uint16_t number_of_sections;
	///The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created.
	uint32_t time_date_stamp;
	///The file offset of the COFF symbol table, or zero if no COFF symbol table is present. This value should be zero for an image because COFF debugging information is deprecated.
	uint32_t pointer_to_symbol_table;
	///The number of entries in the symbol table.
	uint32_t number_of_symbols;
	///The size of the optional header, which is required for executable files but not for object files.
	uint16_t size_of_optional_header;
	///The flags that indicate the attributes of the file. For specific flag values, see Characteristics.
	uint16_t characteristics;
    ///@}
};

///Structure Optional header
struct
#ifdef _MSC_VER
    #pragma packed(push, 1)
#else // _MSC_VER
    __attribute__((packed))
#endif
OptionalHeader
{
    /// @name Optional Header
	///@{

	///The unsigned integer that identifies the state of the image file.
	uint16_t magic;
	///The linker major version number.
	uint8_t  major_linker_version;
	///The linker minor version number.
	uint8_t  minor_linker_version;
	///The size of the code (text) section, or the sum of all code sections if there are multiple sections.
	uint32_t size_of_code;
	///The size of the initialized data section, or the sum of all such sections if there are multiple data sections.
	uint32_t size_of_initialized_data;
	///The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections.
	uint32_t size_of_uninitialized_data;
	///The address of the entry point relative to the image base when the executable file is loaded into memory.
	uint32_t address_of_entry_point;
	///The address that is relative to the image base of the beginning-of-code section when it is loaded into memory.
	uint32_t base_of_code;
	///The preferred address of the first byte of image when loaded into memory; must be a multiple of 64 K.
	uint64_t image_base;
	///The alignment (in bytes) of sections when they are loaded into memory.
	uint32_t section_alignment;
	///The alignment factor (in bytes) that is used to align the raw data of sections in the image file.
	uint32_t file_alignment;
	///The major version number of the required operating system.
	uint16_t major_operating_system_version;
	///The minor version number of the required operating system.
	uint16_t minor_operating_system_version;
	///The major version number of the image.
	uint16_t major_image_version;
	///The minor version number of the image.
	uint16_t minor_image_version;
	///The major version number of the subsystem.
	uint16_t major_subsystem_version;
	///The minor version number of the subsystem.
	uint16_t minor_subsystem_version;
	///Reserved, must be zero.
	uint32_t win32_version_value;
	///The size (in bytes) of the image, including all headers, as the image is loaded in memory.
	uint32_t size_of_image;
	///The combined size of an MS-DOS stub, PE header, and section headers rounded up to a multiple of FileAlignment.
	uint32_t size_of_headers;
	///The image file checksum. The algorithm for computing the checksum is incorporated into IMAGHELP.DLL.
	uint32_t check_sum;
	///The subsystem that is required to run this image.
	uint16_t subsystem;
	///For more information, see DLL Characteristics later in this specification.
	uint16_t dll_characteristics;
	///The size of the stack to reserve.
	uint64_t size_of_stack_reserve;
	///The size of the stack to commit.
	uint64_t size_of_stack_commit;
	///The size of the local heap space to reserve.
	uint64_t size_of_heap_reserve;
	///The size of the local heap space to commit.
	uint64_t size_of_heap_commit;
	///Reserved, must be zero.
	uint32_t loader_flags;
	///The number of data-directory entries in the remainder of the optional header.
	uint32_t number_of_rva_and_sizes;
	///data directory.
	uint64_t data_directory[16];
	///@}
};

/// Structure Section header
struct
#ifdef _MSC_VER
    #pragma packed(push, 1)
#else // _MSC_VER
    __attribute__((packed))
#endif
SectionHeader
{
    /// @name Section Header
	///@{

	///Name
	char name[8];
	///The total size of the section when loaded into memory.
	uint32_t virtual_size;
	///For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory.
	uint32_t virtual_address;
	///Size of raw data
	uint32_t size_of_raw_data;
	///The file pointer to the first page of the section within the COFF file.
	uint32_t pointer_to_raw_data;
	///The file pointer to the beginning of relocation entries for the section.
	uint32_t pointer_to_relocations;
	///The file pointer to the beginning of line-number entries for the section.
	uint32_t pointer_to_linenumbers;
	///The number of relocation entries for the section. This is set to zero for executable images.
	uint16_t number_of_relocations;
	///The number of line-number entries for the section.
	uint16_t number_of_linenumbers;
	///The flags that describe the characteristics of the section. For more information, see Section Flags.
	uint32_t characteristics;
	///@}
};

/// Structure containing PE file data
struct
#ifdef _MSC_VER
    #pragma packed(push, 1)
#else // _MSC_VER
    __attribute__((packed))
#endif
PEFile
{
	/// @name Offsets within file
	///@{

	/// Offset to a file magic
	uint32_t magic_offset;
	/// Offset to a main PE header
	uint32_t header_offset;
	/// Offset to an optional header
	uint32_t optional_header_offset;
	/// Offset to a section table
	uint32_t section_header_offset;
	///@}

	/// @name File headers
	///@{

	/// File magic
	uint32_t magic;
	/// Main header
	struct PEHeader header;
	/// Optional header
	struct OptionalHeader optional_header;
	/// Array of section headers with the size of header.number_of_sections
	struct SectionHeader* section_headers;
	///@}
};

/// @brief Read PE file
/// @param[in] file_input The file to read
/// @param[in] pe_file Structure containing PE file data
/// @return  true in case of success
/// @return  false in case of error
/// @details Reads data from file into a structure
bool read_PE(FILE* file_input, struct PEFile* pe_file);
