/// @file
/// @brief Work with section
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/// @brief Create section headers
/// @param[in] number Number of sections
/// @return  void* Pointer to sections headers
/// @details Frees Allocate memory for sections headers
void* create_section_headers(uint16_t number);

/// @brief Read section headers
/// @param[in] file_input The file to read
/// @param[in] pe_file Structure containing PE file data
/// @return  true in case of success
/// @return  false in case of error
/// @details Reads sections headers from file into a structure
bool read_section_headers(FILE* file_input, struct PEFile* pe_file);

/// @brief Find section
/// @param[in] pe_file Structure containing PE file data
/// @param[in] name Name section
/// @return  section_find Found section
/// @details Search section by name
struct SectionHeader section_find(struct PEFile* pe_file, char* name);

/// @brief Save section
/// @param[in] file_input The file to read
/// @param[in] file_output The file to write
/// @param[in] section_header Section for save
/// @details Writes a section to a file
void save_section(FILE* file_input, FILE* file_output, struct SectionHeader* section_header);
