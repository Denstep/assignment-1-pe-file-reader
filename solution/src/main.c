/// @file
/// @brief Main application file
/// @authors Baranov Denis
#include "pe.h"
#include "sections.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
  (void) argc; (void) argv; // supress 'unused parameters' warning
  usage(stdout);

  FILE* file_input = fopen(argv[1], "rb");
  FILE* file_output = fopen(argv[3], "wb");

  struct PEFile pe_file={0};
  read_PE(file_input, &pe_file);
  pe_file.section_headers = create_section_headers(pe_file.header.number_of_sections);
  read_section_headers(file_input, &pe_file);
  struct SectionHeader desired_section = section_find(&pe_file, argv[2]);
  save_section(file_input, file_output, &desired_section);

  fclose(file_input);
  fclose(file_output);
  free(pe_file.section_headers);

  return 0;
}
