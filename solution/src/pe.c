/// @file
/// @brief PE read structure
#include "pe.h"
#include <inttypes.h>
#include <stdlib.h>

/// @brief Read PE file
/// @param[in] file_input The file to read
/// @param[in] pe_file Structure containing PE file data
/// @return  true in case of success
/// @return  false in case of error
/// @details Reads data from file into a structure
   bool read_PE(FILE* file_input, struct PEFile* pe_file){
    pe_file->magic_offset=0x3C;
    if(fseek(file_input, pe_file->magic_offset, SEEK_SET)!=0){
        return false;
    }
    if(fread(&pe_file->header_offset, sizeof(uint32_t), 1, file_input)!=1){
        return false;
    }
    if(fseek(file_input, pe_file->header_offset, SEEK_SET)!=0){
        return false;
    }

    if(fread(&pe_file->magic, sizeof(uint32_t), 1, file_input)!=1){
        return false;
    }

    if(fread(&pe_file->header, sizeof(struct PEHeader), 1, file_input)!=1){
        return false;
    }

    if(fread(&pe_file->optional_header, sizeof(struct OptionalHeader), 1, file_input)!=1){
        return false;
    }
    return true;
}
