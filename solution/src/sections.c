/// @file
/// @brief Work with section
#include "pe.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// @brief Create section headers
/// @param[in] number Number of sections
/// @return  void* Pointer to sections headers
/// @details Frees Allocate memory for sections headers
void* create_section_headers(uint16_t number) {
	return malloc(sizeof(struct SectionHeader) * number);
}

/// @brief Read section headers
/// @param[in] file_input The file to read
/// @param[in] pe_file Structure containing PE file data
/// @return  true in case of success
/// @return  false in case of error
/// @details Reads sections headers from file into a structure
bool read_section_headers(FILE* file_input, struct PEFile* pe_file){
    for(uint16_t i=0;i<pe_file->header.number_of_sections;i++){
        if(fread(&pe_file->section_headers[i], sizeof(struct SectionHeader), 1, file_input)!=1){
            return false;
        }
    }
    return true;
}

/// @brief Find section
/// @param[in] pe_file Structure containing PE file data
/// @param[in] name Name section
/// @return  section_find Found section
/// @details Search section by name
struct SectionHeader section_find(struct PEFile* pe_file, char* name) {
	for (uint16_t i = 0; i < pe_file->header.number_of_sections; i++) {
        if(strcmp(pe_file->section_headers[i].name, name)==0){
            return pe_file->section_headers[i];
        }
	}
	struct SectionHeader nullsection={0};
	return nullsection;
}

/// @brief Save section
/// @param[in] file_input The file to read
/// @param[in] file_output The file to write
/// @param[in] section_header Section for save
/// @details Writes a section to a file
void save_section(FILE* file_input, FILE* file_output, struct SectionHeader* section_header){
    fseek(file_input, section_header->pointer_to_raw_data, SEEK_SET);
    char *out_section = malloc(section_header->size_of_raw_data);
    fread(out_section, sizeof(char), section_header->size_of_raw_data, file_input);
    fwrite(out_section, sizeof(char), section_header->size_of_raw_data, file_output);
    free(out_section);
}
